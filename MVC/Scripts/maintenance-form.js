﻿var partFields = $('.part-form :input'),
    partTable = $('.part-form tbody'),
    prompt = $('.prompt'),
    totalPartsCost = $('#Cost'),
    placeholder = '<tr class="placeholder"><td colspan="6"><div class="fs-sm">Add a "Part" to calculate service cost.</div><td></tr>',
    part = {};

function addPart(override) {
    var badFields = [];

    $.map(partFields, function (e) {
        if (e.value === null || !e.value) badFields.push(e.name);

        part[e.name] = e.value;
    });

    if (!override && badFields.length !== 0) {
        alert('Please provide a value for field(s): ' + badFields.join(','));
        part = {};
        return;
    }

    $.ajax({
        url: "/Part/AddPart",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify(part),
        success: function (result) {
            if(result.success) {
                if (partTable.children('tr.placeholder').length > 0) partTable.empty();

                partFields.val('');
                totalPartsCost.val(result.totalCost);
                partTable.append(result.html);
            }
        },
        error: function (result) {
            console.log('Error: ' + result.error);
        }
    });
}

function removePart(part) {
    var guid = $(part).parent().parent().attr('id');

    $.ajax({
        url: "/Part/RemovePart",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify({ id: guid.split('_')[1] }),
        success: function (result) {
            partTable.children('#' + guid).remove();
            totalPartsCost.val(result.totalCost);
            
            if(!result.hasParts) partTable.append(placeholder);
        },
        error: function (result) {
            console.log('Error: ' + result.error);
        }
    });
}

$(function () {
    $('#datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker('update', new Date());
});