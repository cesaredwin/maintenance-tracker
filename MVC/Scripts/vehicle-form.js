﻿var years = $("#years"),
    makes = $('#makes'),
    models = $('#models'),
    datePicker = $('#datepicker');

function validate(source) {
    var isValid = false,
  	source = source.val() || '';

    if (source != '' && source != null && source != 'undefined')
        isValid = true;
    return isValid;
}

var validateChange = function (e) {
    var selected = $(e.target),
        isValid = validate(selected),
        dependents = e.data.dependents || [],
        pickFirst = e.data.pickFirst || false,
        cb = e.data.callback ||
            function () {
                console.log('Callback not present. Doing nothing.');
                return;
            };

    $.each(dependents, function (i, item) {
        var obj = $(item);
        if (obj != null) {
            if (pickFirst && obj.is(':disabled') && !isValid == false) {
                obj.prop('disabled', !isValid);
                return false;
            } else {
                obj.prop('disabled', !isValid);
            }
        }
    });

    cb(selected.val(), isValid);
};

$(function () {
    datePicker.datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker('update', new Date());
});