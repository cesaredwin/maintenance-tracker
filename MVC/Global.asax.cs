﻿using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MaintenanceTracker.Core.Data;
using MaintenanceTracker.Core.Modules;
using Ninject;
using Ninject.Web.Common.WebHost;

namespace MaintenanceTracker.MVC
{
    public class MvcApplication : NinjectHttpApplication
    {
        private static IKernel _kernel;

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override IKernel CreateKernel()
        {
            //  Load modules for binding repositories and services.
            _kernel = new StandardKernel(new RepositoryModule(), new ServiceModule());

            //  Share the same datacontext instance with all repositories.
            _kernel.Bind<DbContext>().To<VehicleEntities>().InSingletonScope();

            return _kernel;
        }
    }
}
