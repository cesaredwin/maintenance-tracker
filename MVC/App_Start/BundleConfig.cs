﻿using System.Web.Optimization;

namespace MaintenanceTracker.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //  Scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/forms").Include(
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/default").Include(
                        "~/Scripts/main.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            //  Styles
            bundles.Add(new StyleBundle("~/Content/default").Include(
                        "~/Content/css/bootstrap.css",
                        "~/Content/css/bootstrap-datepicker.css",
                        "~/Content/css/main.css",
                        "~/Content/css/animate.css",
                        "~/Content/css/font-awesome-animation.min.css").Include(
                        "~/Content/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/jquery-ui").Include(
                        "~/Content/css/jquery-ui.min.css"));
        }
    }
}
