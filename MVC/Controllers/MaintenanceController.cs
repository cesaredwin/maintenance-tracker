﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MaintenanceTracker.Core.Models;
using MaintenanceTracker.Core.Services;
using MaintenanceTracker.MVC.ViewModels;

namespace MaintenanceTracker.MVC.Controllers
{
    public class MaintenanceController : BaseController
    {
        #region Fields

        private readonly IMaintenanceService _maintenanceService;
        private readonly IVehicleService _vehicleService;

        #endregion

        #region Constructors

        public MaintenanceController() { }

        public MaintenanceController(IMaintenanceService maintenanceService, IVehicleService vehicleService)
        {
            _maintenanceService = maintenanceService;
            _vehicleService = vehicleService;
        }

        #endregion

        #region Routes

        // GET: Maintenance/Details/5
        public ActionResult Details(Guid id)
        {
            var maintenance = _maintenanceService.FindServiceById(id);

            if (maintenance == null) return HttpNotFound();

            MaintenanceViewModel vm = maintenance;
            return View(vm);
        }

        // GET: Maintenance/Create
        public ActionResult Create(Guid vehicleId)
        {
            if (_vehicleService.FindVehicleById(vehicleId) == null) return HttpNotFound();

            ViewBag.VehicleId = vehicleId;
            Session["parts"] = new List<Part>();

            return View(new MaintenanceViewModel());
        }

        // GET: Maintenance/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Mileage,DateOfService,Description,Cost")] Guid vehicleId, MaintenanceViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);

            var vehicle = _vehicleService.FindVehicleById(vehicleId);
            Maintenance maintenance = vm;
            maintenance.Id = Guid.NewGuid();
            maintenance.Parts = (List<Part>) Session["parts"];

            _maintenanceService.AddMaintenance(vehicleId, maintenance);
            _vehicleService.UpdateVehicle(vehicle);

            return RedirectToAction("Details", "Maintenance", new { id = maintenance.Id });
        }

        // GET: Maintenance/Edit/5
        public ActionResult Edit(Guid id)
        {
            var maintenance = _maintenanceService.FindServiceById(id);

            if (maintenance == null) return HttpNotFound();

            MaintenanceViewModel vm = maintenance;
            return View(vm);
        }

        // GET: Maintenance/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaintenanceId,Mileage,DateOfService,Description,Parts")] MaintenanceViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);

            Maintenance maintenance = vm;

            _maintenanceService.UpdateMaintenance(maintenance);

            return RedirectToAction("Details", "Maintenance", new { id = maintenance.Id });
        }

        // GET: Maintenance/Delete
        public ActionResult Delete(Guid id)
        {
            var maintenance = _maintenanceService.FindServiceById(id);

            if (maintenance == null) return HttpNotFound();

            MaintenanceViewModel vm = maintenance;
            return View(vm);
        }

        // GET: Maintenance/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var vehicleId = _maintenanceService.FindServiceById(id).Vehicle.Id;

            _maintenanceService.RemoveMaintenance(id);

            return RedirectToAction("Details", "Vehicle", new { id = vehicleId });
        }

        #endregion
    }
}
