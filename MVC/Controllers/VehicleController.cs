﻿using System;
using System.Linq;
using System.Web.Mvc;
using MaintenanceTracker.Core.Models;
using MaintenanceTracker.Core.Services;
using MaintenanceTracker.MVC.ViewModels;

namespace MaintenanceTracker.MVC.Controllers
{
    public class VehicleController : BaseController
    {
        #region Fields

        private readonly IVehicleService _vehicleService;

        #endregion

        #region Constructors

        public VehicleController() { }

        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        #endregion

        #region Routes

        // GET: Vehicle
        public ActionResult Index()
        {
            var vehicles = _vehicleService.FindAllVehicles().OrderByDescending(v => v.Year);
            return vehicles.Any() 
                ? View(vehicles.Select(v => (VehicleViewModel)v)) 
                : View("_NoneFound");
        }

        // GET: Vehicle/Details/5
        public ActionResult Details(Guid id)
        {
            var vehicle = _vehicleService.FindVehicleById(id);

            if (vehicle == null) return HttpNotFound();

            VehicleViewModel vm = vehicle;
            return View(vm);
        }

        // GET: Vehicle/Create
        public ActionResult Create()
        {
            return View(new VehicleViewModel
            {
                Makes = _vehicleService.FindAllMakes(),
                Models = _vehicleService.FindAllModels()
            });
        }

        // POST: Vehicle/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VIN,LicensePlate,Year,Make,Model,DatePurchased,PriceAtPurchase,MileageAtPurchase,CurrentMileage,Services")] VehicleViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);

            Vehicle vehicle = vm;
            vehicle.Id = Guid.NewGuid();

            _vehicleService.CreateVehicle(vehicle);

            return RedirectToAction("Index");
        }

        // GET: Vehicle/Edit/5
        public ActionResult Edit(Guid id)
        {
            var vehicle = _vehicleService.FindVehicleById(id);

            if (vehicle == null) return HttpNotFound();

            VehicleViewModel vm = vehicle;
            vm.Makes = _vehicleService.FindAllMakes();
            vm.Models = _vehicleService.FindAllModels().Where(m => m.Make.Description.Equals(vm.Make));

            return View(vm);
        }

        // GET: Vehicle/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleId,VIN,LicensePlate,Year,Make,Model,DatePurchased,PriceAtPurchase,MileageAtPurchase,CurrentMileage")] VehicleViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);

            Vehicle vehicle = vm;
            _vehicleService.UpdateVehicle(vehicle);

            return RedirectToAction("Index");
        }

        // GET: Vehicle/Delete/5
        public ActionResult Delete(Guid id)
        {
            var vehicle = _vehicleService.FindVehicleById(id);

            if (vehicle == null) return HttpNotFound();

            VehicleViewModel vm = vehicle;
            return View(vm);
        }

        // POST: Vehicle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var vehicle = _vehicleService.FindVehicleById(id);

            _vehicleService.DeleteVehicle(vehicle);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult GetModelsForMake(string make)
        {
            var modelsForMake = _vehicleService.FindAllModels()
                                    .Where(m => m.Make.Description.Equals(make) && !m.Description.ToLowerInvariant().Contains("other"))
                                    .Select(m => m.Description);

            return Json(new { models = modelsForMake });
        }

        #endregion
    }
}
