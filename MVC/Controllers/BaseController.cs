﻿using System.Web.Mvc;
using MaintenanceTracker.MVC.Helpers;

namespace MaintenanceTracker.MVC.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            filterContext.Result = View("Error",
                                    new HandleErrorInfo(filterContext.Exception,
                                        filterContext.GetRouteData("controller"),
                                        filterContext.GetRouteData("action")));
        }
    }
}