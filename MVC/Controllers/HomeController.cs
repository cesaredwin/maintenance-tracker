﻿using System.Web.Mvc;

namespace MaintenanceTracker.MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}