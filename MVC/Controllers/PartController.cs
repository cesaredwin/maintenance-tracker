﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MaintenanceTracker.Core.Models;
using MaintenanceTracker.Core.Services;
using MaintenanceTracker.MVC.Helpers;
using MaintenanceTracker.MVC.ViewModels;

namespace MaintenanceTracker.MVC.Controllers
{
    public class PartController : Controller
    {
        #region Fields

        private readonly IMaintenanceService _maintenanceService;

        #endregion

        #region Constructors

        public PartController() { }

        public PartController(IMaintenanceService maintenanceService)
        {
            _maintenanceService = maintenanceService;
        }

        #endregion

        #region Routes

        // GET: Part/Create
        public ActionResult Create(Guid maintenanceId)
        {
            if (_maintenanceService.FindServiceById(maintenanceId) == null) return HttpNotFound();

            ViewBag.MaintenanceId = maintenanceId;

            return View(new PartViewModel());
        }

        // GET: Part/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductName,ModelNumber,Price,Quantity")] Guid maintenanceId, PartViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);

            Part part = vm;

            _maintenanceService.AddPart(maintenanceId, part);

            return RedirectToAction("Details", "Maintenance", new { id = maintenanceId });
        }

        // GET: Part/Edit/5
        public ActionResult Edit(Guid maintenanceId, Guid id)
        {
            var part = _maintenanceService.FindPartById(id);

            if (part == null) return HttpNotFound();

            PartViewModel vm = part;
            ViewBag.MaintenanceId = maintenanceId;

            return View(vm);
        }

        // GET: Part/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,ProductName,ModelNumber,Price,Quantity")] Guid maintenanceId, PartViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);

            Part part = vm;

            _maintenanceService.UpdatePart(maintenanceId, part);

            return RedirectToAction("Details", "Maintenance", new { id = maintenanceId });
        }

        // GET: Part/Delete
        public ActionResult Delete(Guid maintenanceId, Guid id)
        {
            var part = _maintenanceService.FindPartById(id);
            var maintenance = _maintenanceService.FindServiceById(maintenanceId);

            if (part == null || maintenance == null) return HttpNotFound();

            PartViewModel vm = part;
            ViewBag.MaintenanceId = maintenance.Id;

            return View(vm);
        }

        // GET: Part/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid maintenanceId, Guid id)
        {
            _maintenanceService.RemovePart(maintenanceId, id);

            return RedirectToAction("Details", "Maintenance", new { id = maintenanceId });
        }

        #endregion

        public JsonResult AddPart(PartViewModel model)
        {
            var parts = (List<Part>)Session["parts"];
            model.PartId = Guid.NewGuid();
            model.TotalCost = model.Price * model.Quantity;

            parts.Add(model);

            Session["parts"] = parts;

            return Json(new
            {
                success = true,
                html = this.RenderPartialToString("_TableRow", model),
                totalCost = parts.Sum(p => ((PartViewModel)p).TotalCost),
                part = model
            });
        }

        public JsonResult RemovePart(Guid id)
        {
            var parts = (List<Part>)Session["parts"];
            parts.RemoveAll(p => p.Id.Equals(id));

            Session["parts"] = parts;

            return Json(new
            {
                hasParts = parts.Any(),
                totalCost = parts.Any() ? parts.Sum(p => ((PartViewModel)p).TotalCost) : 0m
            });
        }
    }
}