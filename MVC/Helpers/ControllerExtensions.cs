﻿using System.IO;
using System.Web.Mvc;

namespace MaintenanceTracker.MVC.Helpers
{
    public static class ControllerExtensions
    {
        public static string GetRouteData(this ExceptionContext context, string key) => context.RouteData.Values[key].ToString();

        public static string RenderViewToString(this Controller controller, string viewName, object model)
        {
            var context = controller.ControllerContext;
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(context, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public static string RenderPartialToString(this Controller controller, string viewName, object model)
        {
            var context = controller.ControllerContext;

            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            var viewData = new ViewDataDictionary { Model = model };

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}