﻿using MaintenanceTracker.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MaintenanceTracker.MVC.ViewModels
{
    public class MaintenanceViewModel
    {
        #region Properties

        public Guid MaintenanceId { get; set; }
        public Guid VehicleId { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [Range(.01, 999999, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Mileage { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateOfService { get; set; }

        public decimal Cost { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalCost { get; set; }
        public Vehicle Vehicle { get; set; }
        public ICollection<Part> Parts { get; set; }

        #endregion

        #region Methods

        public static implicit operator MaintenanceViewModel(Maintenance maintenance)
        {
            return new MaintenanceViewModel
            {
                MaintenanceId = maintenance.Id,
                VehicleId = maintenance.Vehicle.Id,
                Description = maintenance.Description,
                Mileage = maintenance.Mileage,
                DateOfService = maintenance.DateOfService,
                Cost = maintenance.Cost,
                TotalCost = maintenance.TotalCost,
                Vehicle = maintenance.Vehicle,
                Parts = maintenance.Parts,
            };
        }

        public static implicit operator Maintenance(MaintenanceViewModel vm)
        {
            return new Maintenance
            {
                Id = vm.MaintenanceId,
                Description = vm.Description,
                Mileage = vm.Mileage,
                DateOfService = vm.DateOfService,
                Cost = vm.Cost,
                TotalCost = vm.TotalCost,
                Vehicle = vm.Vehicle,
                Parts = vm.Parts,
            };
        }

        #endregion
    }
}