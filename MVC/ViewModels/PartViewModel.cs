﻿using MaintenanceTracker.Core.Models;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MaintenanceTracker.MVC.ViewModels
{
    public class PartViewModel
    {
        #region Properties

        public Guid PartId { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "Value for {0} must be {1} characters.")]
        [MaxLength(50, ErrorMessage = "Value for {0} cannot be more than {1} characters.")]
        public string ProductName { get; set; }

        [Required]
        [Range(0, 99999, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public decimal Price { get; set; }

        [DisplayName("Model Number")]
        public string ModelNumber { get; set; }

        [Required]
        [Range(0, 99, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Quantity { get; set; }

        public decimal TotalCost { get; set; }

        #endregion

        #region Methods

        public static implicit operator PartViewModel(Part part)
        {
            return new PartViewModel
            {
                PartId = part.Id,
                ProductName = part.Description,
                Price = part.Price,
                ModelNumber = part.ModelNumber,
                Quantity = part.Quantity,
                TotalCost = part.Quantity * part.Price
            };
        }

        public static implicit operator Part(PartViewModel vm)
        {
            return new Part
            {
                Id = vm.PartId,
                Description = vm.ProductName,
                Price = vm.Price,
                ModelNumber = vm.ModelNumber,
                Quantity = vm.Quantity
            };
        }

        #endregion
    }
}