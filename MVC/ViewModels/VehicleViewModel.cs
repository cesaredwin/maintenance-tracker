﻿using MaintenanceTracker.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MaintenanceTracker.MVC.ViewModels
{
    public class VehicleViewModel
    {
        #region Properties

        public Guid VehicleId { get; set; }

        [StringLength(17, ErrorMessage = "Value for {0} must be {1} characters.", MinimumLength = 17)]
        public string VIN { get; set; }

        [Required]
        [DisplayName("License Plate #")]
        [StringLength(7, ErrorMessage = "Value for {0} must be between {2} and {1} characters.", MinimumLength = 6)]
        public string LicensePlate { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        [DisplayName("Date Purchased")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DatePurchased { get; set; }

        [DisplayName("Price At Purchase")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal PriceAtPurchase { get; set; }

        [DisplayName("Mileage at Purchase")]
        public int MileageAtPurchase { get; set; }

        [Required]
        [DisplayName("Current Mileage")]
        public int CurrentMileage { get; set; }

        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal TotalServiceCosts { get; set; }

        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal MaintenanceToVehicleCost => PriceAtPurchase - TotalServiceCosts;

        public int MilesDriven => CurrentMileage - MileageAtPurchase;
        public int NumberOfServices => Services.Count;

        public ICollection<Maintenance> Services { get; set; }
        public IEnumerable<int> Years => Enumerable.Range(1981, DateTime.Now.Year - 1981 + 1).OrderByDescending(y => y);
        public IEnumerable<Make> Makes { get; set; }
        public IEnumerable<Model> Models { get; set; }

        #endregion

        #region Constructors

        public VehicleViewModel()
        {
            Services = new List<Maintenance>();
        }

        #endregion

        #region Methods

        public static implicit operator VehicleViewModel(Vehicle vehicle)
        {
            return new VehicleViewModel
            {
                VehicleId = vehicle.Id,
                VIN = vehicle.VIN,
                LicensePlate = vehicle.LicensePlate,
                Year = vehicle.Year,
                Make = vehicle.Make,
                Model = vehicle.Model,
                DatePurchased = vehicle.DatePurchased,
                PriceAtPurchase = vehicle.PriceAtPurchase,
                MileageAtPurchase = vehicle.MileageAtPurchase,
                CurrentMileage = vehicle.CurrentMileage,
                Services = vehicle.Services,
                TotalServiceCosts = vehicle.Services.Sum(s => s.TotalCost)
            };
        }

        public static implicit operator Vehicle(VehicleViewModel vm)
        {
            return new Vehicle
            {
                Id = vm.VehicleId,
                VIN = vm.VIN,
                LicensePlate = vm.LicensePlate,
                Year = vm.Year,
                Make = vm.Make,
                Model = vm.Model,
                DatePurchased = vm.DatePurchased,
                PriceAtPurchase = vm.PriceAtPurchase,
                MileageAtPurchase = vm.MileageAtPurchase,
                CurrentMileage = vm.CurrentMileage,
                Services = vm.Services
            };
        }

        #endregion
    }
}