# Vehicle Maintenance Tracker
---
Read and record expenses of vehicle maintenance.

## Getting Started
---
Needed to track expenses on maintenance for my vehicles and using Excel is archaic. That's When I decided to create the cleverly named application: **Vehicle Maintenance Tracker**. This WPF application will allow you to read historic information on your vehicle, and record any recent expenses for maintenance on it.

## Prerequisites
---
Entity framework must be installed on the machine.

## Installing
---
1. Update the connection string **VehicleTrackerCS** within the *Core/App.config* and *MVC/Web.config* to point toward an instance of SQL Server on your machine.
2. Within the package manager console run the following command:
```
update-database
```

## Deployment
---
Run application with **MVC** set as start up project.

## Authors
---
* *Edwin Cesar* - Front and back end.

## License
---
Free.
