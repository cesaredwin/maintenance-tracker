namespace MaintenanceTracker.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Maintenance",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                        Mileage = c.Int(nullable: false),
                        DateOfService = c.DateTime(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vehicle_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicle", t => t.Vehicle_Id)
                .Index(t => t.Vehicle_Id);
            
            CreateTable(
                "dbo.Part",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ModelNumber = c.String(),
                        Quantity = c.Int(nullable: false),
                        Maintenance_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Maintenance", t => t.Maintenance_Id)
                .Index(t => t.Maintenance_Id);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        VIN = c.String(),
                        LicensePlate = c.String(),
                        Year = c.Int(nullable: false),
                        Make = c.String(),
                        Model = c.String(),
                        DatePurchased = c.DateTime(nullable: false),
                        PriceAtPurchase = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MileageAtPurchase = c.Int(nullable: false),
                        CurrentMileage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Make",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Model",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                        Make_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Make", t => t.Make_Id)
                .Index(t => t.Make_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Model", "Make_Id", "dbo.Make");
            DropForeignKey("dbo.Maintenance", "Vehicle_Id", "dbo.Vehicle");
            DropForeignKey("dbo.Part", "Maintenance_Id", "dbo.Maintenance");
            DropIndex("dbo.Model", new[] { "Make_Id" });
            DropIndex("dbo.Part", new[] { "Maintenance_Id" });
            DropIndex("dbo.Maintenance", new[] { "Vehicle_Id" });
            DropTable("dbo.Model");
            DropTable("dbo.Make");
            DropTable("dbo.Vehicle");
            DropTable("dbo.Part");
            DropTable("dbo.Maintenance");
        }
    }
}
