namespace MaintenanceTracker.Core.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Data.VehicleEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.VehicleEntities context)
        {
            //  Populate Make and Model tables.
            context.Database.ExecuteSqlCommand(Properties.Resources.makes_and_models);
            base.Seed(context);
        }
    }
}
