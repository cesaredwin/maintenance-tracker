﻿using System;
using MaintenanceTracker.Core.Data;
using MaintenanceTracker.Core.Models;
using System.Linq;

namespace MaintenanceTracker.Core.Services
{
    public interface IMaintenanceService
    {
        #region Methods

        void AddMaintenance(Guid vehicleId, Maintenance maintenance);
        void RemoveMaintenance(Guid maintenanceId);
        void UpdateMaintenance(Maintenance maintenance);
        void AddPart(Guid maintenanceId, Part part);
        void RemovePart(Guid maintenanceId, Guid partId);
        void UpdatePart(Guid maintenanceId, Part part);

        Maintenance FindServiceById(Guid id);
        Part FindPartById(Guid id);

        #endregion
    }

    public class MaintenanceService : IMaintenanceService
    {
        #region Fields

        private readonly IVehicleRepository _vehicleRepository;
        private readonly IMaintenanceRepository _maintenanceRepository;
        private readonly IPartRepository _partRepository;

        #endregion

        #region Constructors

        public MaintenanceService(IVehicleRepository vehicleRepository, IMaintenanceRepository maintenanceRepository, IPartRepository partRepository)
        {
            _vehicleRepository = vehicleRepository;
            _maintenanceRepository = maintenanceRepository;
            _partRepository = partRepository;
        }

        #endregion

        #region IMaintenanceService Members

        public void AddMaintenance(Guid vehicleId, Maintenance maintenance)
        {
            _maintenanceRepository.Create(maintenance);

            var vehicle = _vehicleRepository.FindById(vehicleId);

            vehicle.Services.Add(maintenance);
            _vehicleRepository.Update(vehicle);
        }

        public void RemoveMaintenance(Guid maintenanceId)
        {
            var maintenance = _maintenanceRepository.FindById(maintenanceId);

            maintenance.Parts.ToList().ForEach(p => _partRepository.Delete(p));
            _maintenanceRepository.Delete(maintenance);
        }

        public void UpdateMaintenance(Maintenance maintenance)
        {
            var existing = _maintenanceRepository.FindById(maintenance.Id);
            existing.Description = maintenance.Description;
            existing.Mileage = maintenance.Mileage;
            existing.DateOfService = maintenance.DateOfService;
            existing.Cost = maintenance.Cost;
            existing.TotalCost = maintenance.TotalCost;
            existing.Parts = maintenance.Parts;

            _maintenanceRepository.Update(existing);
        }

        public void AddPart(Guid maintenanceId, Part part)
        {
            var maintenance = _maintenanceRepository.FindById(maintenanceId);

            maintenance.Parts.Add(part);
            _partRepository.Create(part);
            UpdateMaintenance(maintenance);
        }

        public void RemovePart(Guid maintenanceId, Guid partId)
        {
            var maintenance = _maintenanceRepository.FindById(maintenanceId);
            var part = _partRepository.FindById(partId);

            maintenance.Parts.Remove(part);
            _partRepository.Delete(part);
            UpdateMaintenance(maintenance);
        }

        public void UpdatePart(Guid maintenanceId, Part part)
        {
            var maintenance = _maintenanceRepository.FindById(maintenanceId);
            var existingPart = _partRepository.FindById(part.Id);
            existingPart.Description = part.Description;
            existingPart.ModelNumber = part.ModelNumber;
            existingPart.Price = part.Price;
            existingPart.Quantity = part.Quantity;

            _partRepository.Update(part);
            UpdateMaintenance(maintenance);
        }

        public Maintenance FindServiceById(Guid id) => _maintenanceRepository.FindById(id);

        public Part FindPartById(Guid id) => _partRepository.FindById(id);

        #endregion
    }
}
