﻿using MaintenanceTracker.Core.Data;
using MaintenanceTracker.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MaintenanceTracker.Core.Services
{
    public interface IVehicleService
    {
        #region Methods

        void CreateVehicle(Vehicle vehicle);
        void DeleteVehicle(Vehicle vehicle);
        void UpdateVehicle(Vehicle vehicle);

        Vehicle FindVehicleById(Guid id);
        IEnumerable<Vehicle> FindAllVehicles();
        IEnumerable<Model> FindAllModels();
        IEnumerable<Make> FindAllMakes();

        #endregion
    }

    public class VehicleService : IVehicleService
    {
        #region Fields

        private readonly IVehicleRepository _vehicleRepository;
        private readonly IMakeRepository _makeRepository;
        private readonly IModelRepository _modelRepository;

        #endregion

        #region Constructors

        public VehicleService(IVehicleRepository vehicleRepository, IMakeRepository makeRepository, IModelRepository modelRepository)
        {
            _vehicleRepository = vehicleRepository;
            _makeRepository = makeRepository;
            _modelRepository = modelRepository;
        }

        #endregion

        #region IVehicleService Members

        public void CreateVehicle(Vehicle vehicle) => _vehicleRepository.Create(vehicle);

        public void DeleteVehicle(Vehicle vehicle) => _vehicleRepository.Delete(vehicle);

        public void UpdateVehicle(Vehicle vehicle)
        {
            var existing = _vehicleRepository.FindById(vehicle.Id);
            existing.VIN = vehicle.VIN;
            existing.LicensePlate = vehicle.LicensePlate;
            existing.Year = vehicle.Year;
            existing.Make = vehicle.Make;
            existing.Model = vehicle.Model;
            existing.DatePurchased = vehicle.DatePurchased;
            existing.PriceAtPurchase = vehicle.PriceAtPurchase;
            existing.MileageAtPurchase = vehicle.MileageAtPurchase;
            existing.Services = vehicle.Services;

            if ((vehicle.Services != null) && vehicle.Services.Any())
                vehicle.CurrentMileage =
                    vehicle.Services.Max(s => s.Mileage) > vehicle.CurrentMileage
                        ? vehicle.Services.Max(s => s.Mileage)
                        : vehicle.CurrentMileage;

            existing.CurrentMileage = vehicle.CurrentMileage;

            _vehicleRepository.Update(existing);
        }

        public Vehicle FindVehicleById(Guid id) => _vehicleRepository.FindById(id);

        public IEnumerable<Vehicle> FindAllVehicles() => _vehicleRepository.FindAll();

        public IEnumerable<Model> FindAllModels() => _modelRepository.FindAll();

        public IEnumerable<Make> FindAllMakes() => _makeRepository.FindAll();

        #endregion
    }
}
