﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MaintenanceTracker.Core.Models
{
    [Table("Model")]
    public class Model : BaseEntity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public virtual Make Make { get; set; }
    }
}
