﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaintenanceTracker.Core.Models
{
    [Table("Maintenance")]
    public class Maintenance : BaseEntity<Guid>
    {
        #region Properties

        public string Description { get; set; }
        public int Mileage { get; set; }
        public DateTime DateOfService { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }

        public virtual Vehicle Vehicle { get; set; }
        public virtual ICollection<Part> Parts { get; set; }

        #endregion

        #region Constructors

        public Maintenance() { }

        public Maintenance(string desc, int mileage, DateTime dateOfService) : base(Guid.NewGuid())
        {            
            Description = desc;
            Mileage = mileage;
            DateOfService = dateOfService;
        }

        #endregion
    }
}
