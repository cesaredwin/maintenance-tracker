﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaintenanceTracker.Core.Models
{
    [Table("Make")]
    public class Make : BaseEntity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Model> Models { get; set; }
    }
}
