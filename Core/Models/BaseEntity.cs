﻿using MaintenanceTracker.Core.Interfaces;

namespace MaintenanceTracker.Core.Models
{
    public abstract class BaseEntity<T> : IEntity<T> where T : struct
    {
        public T Id { get; set; }

        protected BaseEntity()
        {
        }

        protected BaseEntity(object obj)
        {
            Id = (T)obj;
        }
    }
}
