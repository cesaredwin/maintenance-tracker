﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MaintenanceTracker.Core.Models
{
    [Table("Vehicle")]
    public class Vehicle : BaseEntity<Guid>
    {
        #region Properties

        public string VIN { get; set; }
        public string LicensePlate { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public DateTime DatePurchased { get; set; }
        public decimal PriceAtPurchase { get; set; }
        public int MileageAtPurchase { get; set; }
        public int CurrentMileage { get; set; }

        public decimal AmountInvested => Services != null && Services.Any() ? Services.Sum(s => s.Cost) : 0;

        public virtual ICollection<Maintenance> Services { get; set; }

        #endregion

        #region Constructors

        public Vehicle() { }

        public Vehicle(int year, string make, string model, DateTime datePurchased, decimal priceAtPurchase, int mileageAtPurchase, int? currentMileage = null) : base(Guid.NewGuid())
        {
            Year = year;
            Make = make;
            Model = model;
            DatePurchased = datePurchased;
            PriceAtPurchase = priceAtPurchase;
            MileageAtPurchase = mileageAtPurchase;
            CurrentMileage = currentMileage ?? mileageAtPurchase;
        }

        #endregion
    }
}
