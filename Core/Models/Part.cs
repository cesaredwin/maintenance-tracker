﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MaintenanceTracker.Core.Models
{
    [Table("Part")]
    public class Part : BaseEntity<Guid>
    {
        #region Properties
        
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string ModelNumber { get; set; }
        public int Quantity { get; set; }

        #endregion

        #region Constructors

        public Part() { }

        public Part(string desc, decimal price, string modelNum, int quantity) : base(Guid.NewGuid())
        {
            Description = desc;
            Price = price;
            ModelNumber = modelNum;
            Quantity = quantity;
        }

        #endregion
    }
}
