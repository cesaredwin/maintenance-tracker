﻿using MaintenanceTracker.Core.Data;
using Ninject.Modules;

namespace MaintenanceTracker.Core.Modules
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IVehicleRepository>().To<VehicleRepository>();
            Bind<IMaintenanceRepository>().To<MaintenanceRepository>();
            Bind<IPartRepository>().To<PartRepository>();
            Bind<IModelRepository>().To<ModelRepository>();
            Bind<IMakeRepository>().To<MakeRepository>();
        }
    }
}
