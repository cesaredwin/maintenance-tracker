﻿using MaintenanceTracker.Core.Services;
using Ninject.Modules;

namespace MaintenanceTracker.Core.Modules
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IVehicleService>().To<VehicleService>();
            Bind<IMaintenanceService>().To<MaintenanceService>();
        }
    }
}
