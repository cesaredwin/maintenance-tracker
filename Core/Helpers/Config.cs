﻿using System.Configuration;

namespace MaintenanceTracker.Core.Helpers
{
    internal sealed class Config
    {
        private static Config _instance;

        public string LaptopConnectionString => GetConnectionString("VehicleTrackerLT");
        public string DesktopConnectionString => GetConnectionString("VehicleTrackerDT");

        public Config()
        {
        }

        public static Config Instance => _instance ?? (_instance = new Config());

        private static string GetConnectionString(string csName)
            => ConfigurationManager.ConnectionStrings[csName].ConnectionString;
    }
}
