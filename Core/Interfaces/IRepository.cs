﻿using System.Collections.Generic;

namespace MaintenanceTracker.Core.Interfaces
{
    public interface IRepository<T, in U> where T : IEntity<U> where U : struct
    {
        T FindById(U u);
        IEnumerable<T> FindAll();
        void Create(T t);
        void Update(T t);
        void Delete(T t);
    }
}
