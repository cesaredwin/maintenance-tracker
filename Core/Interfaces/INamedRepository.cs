﻿namespace MaintenanceTracker.Core.Interfaces
{
    public interface INamedRepository<out T>
    {
        #region Methods

        T FindByName(string name);

        #endregion
    }
}
