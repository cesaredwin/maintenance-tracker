﻿using MaintenanceTracker.Core.Interfaces;
using MaintenanceTracker.Core.Models;
using System;
using System.Data.Entity;

namespace MaintenanceTracker.Core.Data
{
    public interface IPartRepository : IRepository<Part, Guid>
    {
    }

    internal class PartRepository : BaseRepository<Part, Guid>, IPartRepository
    {
        public PartRepository(DbContext db) : base(db) { }
    }
}
