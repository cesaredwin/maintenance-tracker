﻿using System.Data.Entity;
using MaintenanceTracker.Core.Models;

namespace MaintenanceTracker.Core.Data
{
    public class VehicleEntities : DbContext
    {
        public VehicleEntities() : base("name=VehicleTrackerCS")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<VehicleEntities>());
        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Make> Makes { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Maintenance> Maintenance { get; set; }
    }
}
