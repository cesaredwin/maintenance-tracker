﻿using System;
using System.Collections.Generic;
using MaintenanceTracker.Core.Interfaces;
using System.Data.Entity;
using System.Linq;
using MaintenanceTracker.Core.Models;
using System.Transactions;

namespace MaintenanceTracker.Core.Data
{
    internal abstract class BaseRepository<T, U> : IRepository<T, U> where T : BaseEntity<U> where U : struct
    {
        #region Fields

        private readonly DbContext _context;
        private readonly DbSet<T> _entities;

        #endregion

        #region Constructor

        protected BaseRepository(DbContext context)
        {
            _context = context;
            _entities = _context.Set<T>();
        }

        #endregion

        #region Methods

        #region IRepository Members

        public virtual T FindById(U u) => _entities.Find(u);

        public virtual IEnumerable<T> FindAll() => _entities.AsEnumerable();

        public virtual void Create(T t) => Transact(() => _entities.Add(t));

        public virtual void Update(T t) => Transact(() =>
        {
            _entities.Attach(t);
            _context.Entry(t).State = EntityState.Modified;
        });

        public virtual void Delete(T t) => Transact(() => _entities.Remove(t));

        #endregion

        protected void Transact(Action action)
        {
            using(var t = new TransactionScope())
            {
                try
                {
                    action();
                    _context.SaveChanges();
                    t.Complete();
                }
                catch (Exception ex)
                {
                    t.Dispose();
                    Console.WriteLine($"Repository method failure due to: {ex.Message}\n\n\n{ex}");
                }
            }
        }

        #endregion
    }
}
