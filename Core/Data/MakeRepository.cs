﻿using MaintenanceTracker.Core.Interfaces;
using MaintenanceTracker.Core.Models;
using System.Data.Entity;
using System.Linq;

namespace MaintenanceTracker.Core.Data
{
    public interface IMakeRepository : IRepository<Make, int>, INamedRepository<Make>
    {
    }

    internal class MakeRepository : BaseRepository<Make, int>, IMakeRepository
    {
        public MakeRepository(DbContext db) : base(db) { }

        public Make FindByName(string code) => FindAll().FirstOrDefault(m => m.Code.Equals(code));
    }
}
