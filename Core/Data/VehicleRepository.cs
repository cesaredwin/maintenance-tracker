﻿using MaintenanceTracker.Core.Interfaces;
using MaintenanceTracker.Core.Models;
using System;
using System.Data.Entity;

namespace MaintenanceTracker.Core.Data
{
    public interface IVehicleRepository : IRepository<Vehicle, Guid>
    {
    }

    internal class VehicleRepository : BaseRepository<Vehicle, Guid>, IVehicleRepository
    {
        public VehicleRepository(DbContext db) : base(db) { }
    }
}
