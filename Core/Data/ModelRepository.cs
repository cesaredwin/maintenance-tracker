﻿using MaintenanceTracker.Core.Interfaces;
using MaintenanceTracker.Core.Models;
using System.Data.Entity;
using System.Linq;

namespace MaintenanceTracker.Core.Data
{
    public interface IModelRepository : IRepository<Model, int>, INamedRepository<Model>
    {
    }

    internal class ModelRepository : BaseRepository<Model, int>, IModelRepository
    {
        public ModelRepository(DbContext db) : base(db) { }

        public Model FindByName(string code) => FindAll().FirstOrDefault(m => m.Code.Equals(code));
    }
}