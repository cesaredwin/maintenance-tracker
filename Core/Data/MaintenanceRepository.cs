﻿using MaintenanceTracker.Core.Interfaces;
using MaintenanceTracker.Core.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace MaintenanceTracker.Core.Data
{
    public interface IMaintenanceRepository : IRepository<Maintenance, Guid>
    {
    }

    internal class MaintenanceRepository : BaseRepository<Maintenance, Guid>, IMaintenanceRepository
    {
        private const decimal TaxRate = 0.05m;

        public MaintenanceRepository(DbContext db) : base(db) { }

        #region Methods

        #region Overrides

        public override void Create(Maintenance maintenance)
        {
            maintenance = adjustExpenses(maintenance);
            base.Create(maintenance);
        }

        public override void Update(Maintenance maintenance)
        {
            maintenance = adjustExpenses(maintenance);
            base.Update(maintenance);
        }

        #endregion

        private Maintenance adjustExpenses(Maintenance maintenance)
        {
            if ((maintenance.Parts == null) || !maintenance.Parts.Any()) return maintenance;

            maintenance.Cost = maintenance.Parts.Sum(p => p.Price * p.Quantity);
            maintenance.TotalCost = maintenance.Cost * TaxRate + maintenance.Cost;

            return maintenance;
        }

        #endregion
    }
}
